#cloud-config
write_files:
  - path: "/etc/hstack/hstack.conf"
    permissions: "0644"
    owner: "root"
    content: | 
      PRIVATE_NETWORK="${ private_network }"

  - path: "/etc/hstack/consul.conf"
    permissions: "0644"
    owner: "root"
    content: | 
      DOMAIN=${ domain }
      DATACENTER=${ dc }
      CONSUL_MODE="${ consul_mode }"
      CONSUL_AUTOJOIN="aws"
      CONSUL_AUTOJOIN_AWS_TAG_VALUE="${ autojoin_tag_value }"
      CONSUL_AUTOJOIN_AWS_TAG_KEY="${ autojoin_tag_key }"
      CONSUL_AGENT_TAGS="${ tags }"
      CONSUL_ENCRYPT_KEY="${ consul_encrypt_key }"
      JOIN_IPV4_ADDR="${ join_ipv4_addr }"
      JOIN_IPV4_ADDR_WAN="${ join_ipv4_addr_wan }"
      CONSUL_BOOTSTRAP_EXPECT=${ consul_bootstrap_expect }
      CONSUL_OPTS="${ consul_opts }"
      NTP_MODE="${ntp_mode}"

  - path: "/etc/hstack/nomad.conf"
    permissions: "0644"
    owner: "root"
    content: | 
      NOMAD_MODE="${ nomad_mode }"
      NOMAD_BOOTSTRAP_EXPECT=${ nomad_bootstrap_expect }
      NOMAD_ENCRYPT_KEY="${ nomad_encrypt_key }"
      NOMAD_NODE_CLASS="${ nomad_node_class }"

  - path: "/etc/hstack/vault.conf"
    permissions: "0644"
    owner: "root"
    content: | 
      VAULT_MODE="${ vault_mode }"
      VAULT_PGP_KEYS="${ vault_pgp_keys }"
      VAULT_KEY_THRESHOLD=${ vault_key_threshold }

  - path: "/etc/hstack/docker_registry.conf"
    permissions: "0644"
    owner: "root"
    content: |
      REGISTRY_MODE="${docker_registry_mode}"
      REGISTRY_STORAGE_PROVIDER=s3
      REGISTRY_HTTP_SECRET="${docker_registry_http_secret}"
      REGISTRY_STORAGE_S3_REGION="${docker_registry_storage_s3_region}"
      REGISTRY_STORAGE_S3_ACCESSKEY="${docker_registry_storage_s3_accesskey}"
      REGISTRY_STORAGE_S3_SECRETKEY="${docker_registry_storage_s3_secretkey}"
      REGISTRY_STORAGE_S3_BUCKET="${docker_registry_storage_s3_bucket}"
      REGISTRY_STORAGE_S3_ROOTDIRECTORY="${docker_registry_storage_s3_rootdirectory}"

  - path: "/etc/ssl/certs/rootca.pem"
    permissions: "0644"
    owner: "root"
    encoding: base64
    content: ${root_ca}
