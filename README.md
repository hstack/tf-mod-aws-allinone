IMPORTANT
=========

once vault servers are up, you still have to manually finish the bootstrap process.

- AUTO   : a consul agent is started and listening on http://127.0.0.1:8500 and will be used as a storage backend by vault
- AUTO   : vault is started and listening on http://127.0.0.1:8200. Yet vault hasn't been initialized.
- MANUAL : one has to ssh to one ssh server and run the `/opt/hstack/vault-manage init` command to initialize vault with the given keybase handles and key threshold. Yet vault is still in a sealed state.
- MANUAL : given the keybase handles and the key threshold, people have to ssh to every vault server and unseal them.

```
core@ip-10-100-85-90 ~ $ /opt/hstack/consul kv get vault/unseal-key-keybase-HANDLE&
sflksj234FSDFS.....
...slkdfjsldkfjsldkfjsf==

user@workstation ~ $ echo "sflksj234FSDFS... ...slkdfjsldkfjsldkfjsf==" | base64 -d | keybase pgp decrypt
azeiorpziofsjfsdkjfhskdjhfksjdhfsdkjfhskcncs23f324lkj%

core@ip-10-100-85-90 ~ $ /opt/hstack/vault unseal -address=http://127.0.0.1:8200

Key (will be hidden):
Sealed: false
Key Shares: 1
Key Threshold: 1
Unseal Progress: 0
Unseal Nonce:
$ 
```

- AUTO   : once vault is unsealed, consul watchers will achieve the vault configuration, setup a pki backend and remove the root_token, consul agents and vault servers  
           will then be restarted with a proper TLS configuration based on a self signed autogen ca cert.
- MANUAL : as the vault servers have been restarted, given the keybase handles and the key threshold, people have to ssh to every vault server and unseal them once again.
