variable "name"                 { default = "hstack" }
variable "hstack_version"       { default = "v1.0"}
variable "atlas_username"       { default = "hstack" }
variable "region"               { default = "us-east-1" }
variable "count"                { default = "-1" }
variable "instance_type"        { default = "t2.medium" }
variable "iam_instance_profile" { default = "consul_node" }
variable "subnet_ids"           { }
variable "keypair_name"         { }
variable "security_group_ids"   { default = "" }

variable "domain"               { default = "consul" }
variable "dc"                   { default = "dc1" }

variable "consul_mode"          { default = "agent" }
variable "vault_mode"           { default = "off" }
variable "nomad_mode"           { default = "off" }
variable "docker_registry_mode" { default = "off" }

variable "join_ipv4_addr"          { default = "" }
variable "join_ipv4_addr_wan"      { default = "" }
variable "autojoin_tag_value"      { default = "" }
variable "autojoin_tag_key"        { default = "consul_autojoin" }
variable "consul_bootstrap_expect" { default = "" }
variable "consul_encrypt_key"      { }

# nomad encrypt key is not mandatory for client nodes
variable "nomad_encrypt_key"      { default = "" }
variable "nomad_bootstrap_expect" { default = "" }
variable "nomad_node_class"       { default = "standard" }


variable "vault_pgp_keys"       { default = "" }
variable "vault_key_threshold"  { default = "3" }

variable "tags"                 { default = "" }
variable "root_ca"              { default = "" }

variable "docker_registry_http_secret"              { default = "" }
variable "docker_registry_storage_s3_region"        { default = "" }
variable "docker_registry_storage_s3_accesskey"     { default = "" }
variable "docker_registry_storage_s3_secretkey"     { default = "" }
variable "docker_registry_storage_s3_bucket"        { default = "" }
variable "docker_registry_storage_s3_rootdirectory" { default = "/" }
