variable "aws_region"     { default = "us-east-1" }
variable "name"           { default = "hstack-test" }
variable "bastion_count"  { default = "1" }
variable "hstack_version" { default = "6" }

variable "domain" { default = "test" }
variable "dc"     { default = "nomad" }

variable "autojoin_tag_value" { default = "hstackconsuljoin"}
variable "docker_registry_s3_bucket" { default = "hstack-docker-registry" }
variable "docker_registry_s3_rootdirectory" { default = "/" }
variable "docker_registry_http_secret" { }

variable "keybase_handles"     { }
variable "encrypt_key"         { }
variable "keypair_name"        { }

provider "aws" {
  region = "${var.aws_region}"
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com", "s3.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "s3_bucket_registry" {
    name = "${var.name}-s3_bucket_registry"
    path = "/"
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket",
        "s3:GetBucketLocation",
        "s3:ListBucketMultipartUploads"
      ],
      "Resource": "arn:aws:s3:::${var.docker_registry_s3_bucket}"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject",
        "s3:ListMultipartUploadParts",
        "s3:AbortMultipartUpload"
      ],
      "Resource": "arn:aws:s3:::${var.docker_registry_s3_bucket}/*"
    }
  ]
}
    EOF
}

resource "aws_iam_policy" "consul_discovery" {
  name = "${var.name}-consul-discovery"
  path = "/"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1468377974000",
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeAutoScalingGroups",
                "ec2:DescribeInstances"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
    EOF
}

resource "aws_iam_role" "consul_node" {
    name = "${var.name}-consul-node"
    assume_role_policy = "${data.aws_iam_policy_document.assume_role_policy.json}"
}

resource "aws_iam_role_policy_attachment" "consul_node_discovery" {
    role = "${aws_iam_role.consul_node.name}"
    policy_arn = "${aws_iam_policy.consul_discovery.arn}"
}

resource "aws_iam_role_policy_attachment" "s3_bucket_registry" {
  role = "${aws_iam_role.consul_node.name}"
  policy_arn = "${aws_iam_policy.s3_bucket_registry.arn}"
}

resource "aws_iam_instance_profile" "consul_node" {
    name = "${var.name}-consul-node"
    role = "${aws_iam_role.consul_node.name}"
}

module "net" {
   source = "git::https://gitlab.com/hstack/tf-mod-aws-network.git"

   name = "${var.name}-net"

   region = "${var.aws_region}"
   bastion_count = "${var.bastion_count}"
   keypair_name = "${var.keypair_name}"
   hstack_version = "${var.hstack_version}"
   nat_count = "1"
}

module "servers" {
  source = "git::https://gitlab.com/hstack/tf-mod-aws-allinone.git"

  name = "${var.name}-allinone"

  hstack_version = "${var.hstack_version}"
  keypair_name = "${var.keypair_name}"
  region = "${var.aws_region}"
  iam_instance_profile = "${aws_iam_instance_profile.consul_node.name}"

  domain = "${var.domain}"
  dc = "${var.dc}"

  consul_mode = "server"
  nomad_mode = "server"
  docker_registry_mode = "server"

  subnet_ids = "${module.net.private_subnet_ids}"
  count = "3"

  autojoin_tag_value = "${var.autojoin_tag_value}"

  consul_encrypt_key = "${var.encrypt_key}"
  consul_bootstrap_expect = "2"

  nomad_encrypt_key = "${var.encrypt_key}"
  nomad_bootstrap_expect = "2"

  vault_pgp_keys = "${var.keybase_handles}"
  vault_key_threshold = "1"

  docker_registry_http_secret = "${var.docker_registry_http_secret}"
  docker_registry_storage_s3_region = "${var.aws_region}"
  docker_registry_storage_s3_bucket = "${var.docker_registry_s3_bucket}"
  docker_registry_storage_s3_rootdirectory = "${var.docker_registry_s3_rootdirectory}"

}

output "configuration" {
  value = <<CONFIGURATION

* Add your private key and SSH into any private node via the Bastion host:
  $$ ssh-add path_to_private_sshkey
  $$ ssh -A core@${module.net.bastion_public_dns}

* Add these lines to your ~/.ssh/config file to connect to your instances through the bastion:
   ---
   Host "${replace(replace(module.net.vpc_cidr,".0",""),"/\\/[\\d]+/",".*")}"
      IdentityFile path_to_private_sshkey
      User core
      ProxyCommand ssh -l core -i path_to_private_sshkey -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null "${module.net.bastion_public_dns}" ncat %h %p
   ---
CONFIGURATION
}
