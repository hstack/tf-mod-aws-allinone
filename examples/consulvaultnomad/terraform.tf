variable "aws_region"     { default = "us-east-1" }
variable "name"           { default = "hstack-test" }
variable "bastion_count"  { default = "1" }
variable "hstack_version" { default = "6" }

variable "domain" { default = "test" }
variable "dc"     { default = "allinone" }

variable "keybase_handles"     { }
variable "encrypt_key"         { }
variable "keypair_name"        { }

variable "autojoin_tag_value" { default = "hstackconsuljoin"}

provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_iam_policy" "consul_discovery" {
    name = "${var.name}-consul-discovery"
    path = "/"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1468377974000",
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeAutoScalingGroups",
                "ec2:DescribeInstances"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
    EOF
}

resource "aws_iam_role" "consul_node" {
    name = "${var.name}-consul-node"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "consul_node_discovery" {
    name = "${var.name}-consul-discovery"
    roles = ["${aws_iam_role.consul_node.name}"]
    policy_arn = "${aws_iam_policy.consul_discovery.arn}"
}

resource "aws_iam_instance_profile" "consul_node" {
    name = "${var.name}-consul-node"
    role = "${aws_iam_role.consul_node.name}"
}

module "net" {
   source = "git::https://gitlab.com/hstack/tf-mod-aws-network.git"

   name = "${var.name}-net"

   region = "${var.aws_region}"
   bastion_count = "${var.bastion_count}"
   keypair_name = "${var.keypair_name}"
   hstack_version = "${var.hstack_version}"
   nat_count = "1"
}

module "allinone" {
  source = "git::https://gitlab.com/hstack/tf-mod-aws-allinone.git"

  name = "${var.name}-allinone"

  hstack_version = "${var.hstack_version}"
  keypair_name = "${var.keypair_name}"
  region = "${var.aws_region}"
  iam_instance_profile = "${aws_iam_instance_profile.consul_node.name}"

  domain = "${var.domain}"
  dc = "${var.dc}"

  consul_mode = "server"
  nomad_mode = "both"
  vault_mode = "server"

  subnet_ids = "${module.net.private_subnet_ids}"
  count = "3"

  autojoin_tag_value = "${var.autojoin_tag_value}"

  consul_encrypt_key = "${var.encrypt_key}"
  consul_bootstrap_expect = "2"

  nomad_encrypt_key = "${var.encrypt_key}"
  nomad_bootstrap_expect = "2"

  vault_pgp_keys = "${var.keybase_handles}"
  vault_key_threshold = "1"

}

output "configuration" {
  value = <<CONFIGURATION

* Add your private key and SSH into any private node via the Bastion host:
  $$ ssh-add path_to_private_sshkey
  $$ ssh -A core@${module.net.bastion_public_dns}

* Add these lines to your ~/.ssh/config file to connect to your instances through the bastion:
   ---
   Host "${replace(replace(module.net.vpc_cidr,".0",""),"/\\/[\\d]+/",".*")}"
      IdentityFile path_to_private_sshkey
      User core
      ProxyCommand ssh -l core -i path_to_private_sshkey -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null "${module.net.bastion_public_dns}" ncat %h %p
   ---
CONFIGURATION
}
