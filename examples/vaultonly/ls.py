#!/usr/bin/env python

import boto3
import os
import sys

if len(sys.argv) < 1:
    print( "usage: {0} [tag name prefix]; Ex: {0} \"MyStack*\"")
    sys.exit(1)


if len(sys.argv) == 1:
    filter = "*"
else:
    filter = sys.argv[1]

profile = os.environ.get('AWS_PROFILE')
print("> working with profile: {0}".format(profile))
session = boto3.session.Session(profile_name=profile)
ec2 = session.resource('ec2')
ec2_client = session.client('ec2')

for i in ec2.instances.filter(Filters=[{'Name':'tag:Name', 'Values':[filter]}]):
    if i.tags is None:
        continue
    for t in i.tags:
        if t['Key'] == 'Name':
            instance_name = t['Value']

    print("%s %s #(id:%s, type:%s, state:%s)" % (i.private_ip_address, instance_name, i.id, i.instance_type, i.state['Name']))
