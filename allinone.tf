#--------------------------------------------------------------
# This module creates all resources necessary for hstack
#--------------------------------------------------------------

data "atlas_artifact" "hstack_ami" {
  name = "${var.atlas_username}/aws-${var.region}-coreos-hstack"

  type = "amazon.image"
  metadata {
     version = "${var.hstack_version}"
  }
}

data "aws_subnet" "selected" {
  id = "${element(split(",",var.subnet_ids), 0)}"
}

data "aws_vpc" "selected" {
  id = "${data.aws_subnet.selected.vpc_id}"
}

data "template_file" "hstack_user_data" {
  template = "${file("${path.module}/hstack.tpl")}"

  vars {
    private_network         = "${data.aws_vpc.selected.cidr_block}"

    consul_mode             = "${var.consul_mode}"
    vault_mode              = "${var.vault_mode}"
    nomad_mode              = "${var.nomad_mode}"
    docker_registry_mode    = "${var.docker_registry_mode}"

    # consul servers play the role of ntp servers
    ntp_mode                = "${var.consul_mode == "server" ? "server" : "" }"

    join_ipv4_addr          = "${var.join_ipv4_addr}"
    join_ipv4_addr_wan      = "${var.join_ipv4_addr_wan}"
    autojoin_tag_key        = "${var.autojoin_tag_key}"
    autojoin_tag_value      = "${coalesce(var.autojoin_tag_value, var.name)}"
    domain                  = "${var.domain}"
    dc                      = "${var.dc}"
    consul_encrypt_key      = "${var.consul_encrypt_key}"
    consul_bootstrap_expect = "${var.consul_bootstrap_expect}"
    consul_opts             = "${var.consul_mode == "server" ? "-ui" : "" }"

    root_ca                 = "${base64encode(var.root_ca)}"
    nomad_encrypt_key       = "${var.nomad_encrypt_key}"
    nomad_bootstrap_expect  = "${var.nomad_bootstrap_expect}"
    nomad_node_class        = "${var.nomad_node_class}"

    vault_pgp_keys          = "${var.vault_pgp_keys}"
    vault_key_threshold     = "${var.vault_key_threshold}"

    docker_registry_http_secret              = "${var.docker_registry_http_secret}"
    docker_registry_storage_s3_region        = "${coalesce(var.docker_registry_storage_s3_region,var.region)}"
    docker_registry_storage_s3_accesskey     = "${var.docker_registry_storage_s3_accesskey}"
    docker_registry_storage_s3_secretkey     = "${var.docker_registry_storage_s3_secretkey}"
    docker_registry_storage_s3_bucket        = "${var.docker_registry_storage_s3_bucket}"
    docker_registry_storage_s3_rootdirectory = "${var.docker_registry_storage_s3_rootdirectory}"

    tags                    = "${var.tags}"
  }
}

resource "aws_security_group" "sg" {
  count =  "${var.count > 0 && var.security_group_ids == "" ? 1 : 0}"

  name        = "${var.name}"
  vpc_id      = "${data.aws_vpc.selected.id}"
  description = "Security group for Consul"

  tags      { Name = "${var.name}" }
  lifecycle { create_before_destroy = true }

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["${data.aws_vpc.selected.cidr_block}"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_launch_configuration" "hstack" {
  count =  "${var.count> 0 ? 1 : 0}"

  name_prefix                 = "${var.name}"
  iam_instance_profile        = "${var.iam_instance_profile}"
  image_id                    = "${lookup(data.atlas_artifact.hstack_ami.metadata_full, format("region-%s", var.region))}"
  instance_type               = "${var.instance_type}"
  security_groups             = [ "${coalescelist(compact(split(",", var.security_group_ids)), list(aws_security_group.sg.id))}" ]

  associate_public_ip_address = "false"
  key_name                    = "${var.keypair_name}"

  user_data                   = "${data.template_file.hstack_user_data.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

//  Auto-scaling group for our cluster.
resource "aws_autoscaling_group" "hstack" {
  count =  "${var.count> 0 ? 1 : 0}"

  name_prefix = "${var.name}"
  launch_configuration = "${aws_launch_configuration.hstack.name}"
  desired_capacity     = "${var.count}"
  min_size             = "${var.count}"
  max_size             = "${var.count}"
  health_check_grace_period = "60"
  health_check_type         = "EC2"
  force_delete              = false
  wait_for_capacity_timeout = 0

  vpc_zone_identifier = [ "${split(",",var.subnet_ids)}"]

  enabled_metrics           = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances"
  ]

  tag {
    key                 = "Name"
    value               = "${var.name}"
    propagate_at_launch = true
  }

  tag {
    key                 = "${var.autojoin_tag_key}"
    value               = "${coalesce(var.autojoin_tag_value, var.name)}"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
